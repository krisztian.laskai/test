describe('Onboarding - wrong login', function () {
// we can use these values to log in
    const url = 'https://docs.a38.hu/onboarding/'
    const username = 'fake@email.com'
    const password = 'wrong_password'

    context('HTML form submission', function () {
        beforeEach(function () {
            cy.visit(url)
        })

        it('displays errors on login', function () {
            // incorrect username on purpose
            cy.get('input[name=username]').type(username)
            cy.get('input[name=password]').type(password)
            // Todo: Primary click is a must!
            //      cy.get('.primary').click()

            // Todo: we should have visible errors now
            //     cy.get('.error').should('be.visible')
            // and still be on the same URL
            cy.url().should('include', url)
            cy.screenshot('wrong-login')
        })
    })
})