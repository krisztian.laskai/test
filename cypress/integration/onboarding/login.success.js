describe('Onboarding - success login', function () {
// we can use these values to log in
    const url = 'https://docs.a38.hu/onboarding/'
    const username = 'developer'
    const password = 'A38developer'

    context('HTML form submission', function () {
        beforeEach(function () {
            cy.visit(url)
        })

        it('displays errors on login', function () {
            // incorrect username on purpose
            cy.get('input[name=username]').type(username)
            cy.get('input[name=password]').type(password)
            cy.get('.primary').click()

            cy.screenshot('success-login')

            // we should see search
            cy.get('#search-by').should('be.visible')
            cy.get('[data-nav-id="/form"]').click()
            cy.get('input[name="data[feladat]"]').should('be.visible')
            cy.get('input[name="data[feladat]"]').type('hello')

            cy.screenshot('teszt')
        })
    })
})